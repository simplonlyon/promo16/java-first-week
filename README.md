# Exo bilan semaine 1

## I. Des ptites variables dans une ptite classe
1. Dans le App.java, créer quelques variables et les afficher avec un sysout : une variable qui contiendra votre age, une variable qui contiendra le nom de la promo, une variable qui contiendra si c'est vrai ou faux que vous aimez java, une variable qui contiendra un tableau avec les langages que vous connaissez dedans
2. Afin de ne pas trop polluer le main, créer une classe bases.Variables dans laquelle vous mettrez une méthode first() avec toutes vos variables et vos sysout créées avant dedans
3. Instancier cette classe dans le main et y appeler la méthode first
4. Dans la classe Variables, rajouter une méthode withParameters qui attendra en argument une String et un int et qui fera un sysout des deux
5. Dans la classe Variables, rajouter une méthode withReturn qui renverra une chaîne de caractère "Le return" et faire en sorte de l'afficher dans la méthode main de App.java 

## II. Conditions et méthodes 
1. Créer une nouvelle classe bases.Conditions et dedans créer une méthode isPositive qui va attendre un int en argument et renverra true ou false selon si le int est posifit ou négatif
2. Créer une méthode skynetAI qui attendra une chaîne de caractères en paramètre et qui renverra "Hello how are you ?" si le paramètre est égal à "Hello", "I'm fine, thank you" si le paramètre est égal à "How are you ?", "Goodbye friend" si le paramètre est égal à "Goodbye" et renverra "I don't understand" pour n'importe quelle autre valeur
3. Toujours dans Conditions, créer une méthode buy qui attendra un age et un produit en String en argument, et faire une condition qui fait que si le produit est "Alcohol" et que l'age est inférieur à 18, ça affiche un message de refus, et qui affiche un message d'acceptation pour tous les autres cas
4. Encore dans conditions, créer une méthode greater qui attendra 2 int en argument et qui fera un return de l'int le plus grand des deux

## III. Classes, propriété, methodess
1. Créer une classe oop.Laptop qui aura une propriété model en String, une propriété battery en int initialisée à 50, une propriété turnedOn  booléenne initialisée à false et une propriété pluggedIn booléenne initialisée à false. Faire un constructeur pour cette classe et en faire une instance
2. Créer une méthode plug() qui va passer la propriété pluggedIn à true et qui augmentera la batterie de 5
3. Créer une méthode powerSwitch() qui :
   * Si turnedOn est true => passe turnedOn à false
   * Si turnedOn est false et que pluggedIn est true => passe turnedOn à true
   * Si turnedOn est false, que pluggedIn est false et que battery est suprieur à 10 => passe turnedOn à true et retire 5 à battery
   * Sinon, ne fait rien
4. Rajouter une propriété OS en String qui par défaut sera null, puis créer une méthode install qui attendra l'OS à installer en argument et qui ne le fera que si turnedOn est true (et fera perdre 5 de battery si pas pluggedIn)
5. Et du coup vu qu'on fait cette vérif' souvent, faire une méthode privée consumeBattery qui enlèvera 5 de battery si pas pluggedIn, rajoutera 5 de battery si pluggedIn, et si jamais il arrive à 0, passera turnedOn à false, et du coup faire en sorte d'appeler cette méthode là où on faisait le consommer à la main (et aussi faire que la battery puisse pas dépasser 100)

## IV. Boucles et array
1. Faire une nouvelle classe base.Looping et dedans créer une méthode qui va faire une boucle, et à chaque tour de boucle va répéter un mot plusieurs fois 
2. Dans la classe Looping, créer une méthode findMax qui va attendre un array ou une list de int en entrée (à vous de choisir) et qui va parcourir le tableau pour trouver le int le plus grand (ya ptêt déjà une méthode de list qui le fait, je sais pas, mais si oui, l'utilisez pas pour cet exo). Aide :  pour ça, il y aura besoin d'une variable à l'extérieur de la boucle, et dans la boucle vérifié si l'élément actuel est plus grand que cette variable, si oui, on met l'élément dans la variable, et à la fin c'est cette variable qu'on return 
3. Toujours dans Looping créer une méthode find  qui va attendre un array de String[] et un String à rechercher en arguments, et qui va utiliser une boucle pour parcourir le tableau et renvoyer true si le mot à recherché et trouvé, et false s'il ne l'est pas à la fin de la boucle (ça clairement ya une méthode de list qui existe pour le faire, mais donc l'idée c'est de le faire sois même là)
4. Dans Looping, créer une méthode qui va attendre en argument une String et qui va compter et afficher le nombre de mot dans la console, puis utiliser une boucle pour faire la moyenne du nombre de lettre par mot 
