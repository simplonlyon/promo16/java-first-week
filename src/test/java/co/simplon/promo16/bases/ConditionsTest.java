package co.simplon.promo16.bases;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ConditionsTest {
    @Test
    public void testSkynetAI() {

        Conditions instance = new Conditions();
        
        assertEquals("Hello how are you ?", instance.skynetAI("Hello"));
        assertEquals("Goodbye friend", instance.skynetAI("Goodbye"));
        assertEquals("I'm fine thank you", instance.skynetAI("How are you ?"));
        assertEquals("I don't understand, sorry", instance.skynetAI("autre chose"));
    }
}
