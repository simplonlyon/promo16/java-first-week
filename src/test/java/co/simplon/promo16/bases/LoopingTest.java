package co.simplon.promo16.bases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class LoopingTest {
    private Looping looping;

    @Before
    public void init() {
        looping = new Looping();
    }

    @Test
    public void shouldReturnTheAverageCharCountOfString() {

        assertEquals(4, looping.averageChars("blup blop blap"));
        assertEquals(3, looping.averageChars("bloup oui non"));
    }

    @Test
    public void shouldReturnTrueIfContainsWord() {
        String[] words = {"Test", "Test2", "Test3"};
        assertTrue(looping.find(words, "Test2"));

    }

    @Test
    public void shouldReturnFalseIfNotContainsWord() {
        String[] words = {"Test", "Test2", "Test3"};
        assertFalse(looping.find(words, "Testouille"));

    }

    @Test
    public void shouldReturnTheHighestNumber() {
        
        assertEquals(10, looping.findMax(List.of(1,2,3,10)));
        assertEquals(12, looping.findMax(List.of(12,2,3,10)));
        assertEquals(2, looping.findMax(List.of(-2,2)));
        assertNotEquals(3, looping.findMax(List.of(2,3,10))); //mais bon
        
    }
}
