package co.simplon.promo16.bases;

public class Conditions {
    /**
     * Une méthode qui envoie une réponse différente selon la phrase qu'on lui donne
     * en entrée
     * @param sentence la phrase d'entrée
     * @return La réponse selon ce qu'on lui a dit
     */
    public String skynetAI(String sentence) {
        String greeting = "Hello";
        String leaving = "Goodbye";
        String feeling = "How are you ?";

        if(sentence == greeting) {
            return "Hello how are you ?";
        }
        if(sentence == leaving) {
            return "Goodbye friend";
        }
        if(sentence == feeling) {
            return "I'm fine thank you";
        }


        return "I don't understand, sorry";
    }
    
}
