package co.simplon.promo16.bases;

import java.util.List;

public class Looping {

    /**
     * Méthode qui trouve le nombre le plus élevé dans un tableau de nombres
     * 
     * @param numbers Le tableau de nombre
     * @return Le nombre le plus élevé
     */
    public int findMax(List<Integer> numbers) {
        int maxNumber = numbers.get(0);
        for (int i = 1; i < numbers.size(); i++) {
            if (numbers.get(i) > maxNumber) {
                maxNumber = numbers.get(i);
            }
        }

        // Integer maxNumber = null;
        // for (Integer number : numbers) {
        // if(number == null || number > maxNumber) {
        // maxNumber = number;
        // }
        // }

        return maxNumber;
    }

    /**
     * Méthode qui vérifie si une chaîne de caractères est contenue dans un tableau
     * 
     * @param words  Le tableau de String dans lequel on cherche
     * @param toFind Le mot qu'on cherche
     * @return True si trouvé, false si non
     */
    public boolean find(String[] words, String toFind) {
        for (String word : words) {
            if (word == toFind) {
                return true;
            }
        }
        return false;
    }

    /**
     * Méthode qui va faire la moyenne de lettre par mot dans une phrase
     * 
     * @param sentence La phrase avec les mots séparés par des espaces
     * @return Le nombre moyen de caractères par mot
     */
    public int averageChars(String sentence) {
        String[] wordArray = sentence.split(" ");
        System.out.println(wordArray.length);
        int lettersCount = 0;
        for (String word : wordArray) {
            lettersCount += word.length();
        }
        if (lettersCount > 0) {
            return lettersCount / wordArray.length;
        }
        return 0;
    }
}
