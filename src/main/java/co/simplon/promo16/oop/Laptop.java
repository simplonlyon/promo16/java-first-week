package co.simplon.promo16.oop;

public class Laptop {
    private String model;
    private int battery = 50;
    private boolean turnedOn = false;
    private boolean pluggedIn = false;
    
    public Laptop(String model) {
        this.model = model;
    }
    /**
     * Méthode qui branche le portable
     */
    public void plug() {
        pluggedIn = true;
        battery += 5;
    }
    /**
     * Méthode qui allume ou éteint le pc selon l'état actuel du laptop
     */
    public void powerSwitch() {
        if(turnedOn == true) {
            turnedOn = false;
            return;
        }
        if(turnedOn == false && pluggedIn == true) {
            turnedOn = true;
            return;
        }
        if(!turnedOn && !pluggedIn && battery >10) {
            turnedOn = true;
            battery-=5;
            return;
        }
        //Autre manière de faire avec des else if tout aussi valide
        // if(turnedOn == true) {
        //     turnedOn = false;
        // } else if(pluggedIn == true) {
        //     turnedOn = true;
        

        // } else  if(battery >10) {
        //     turnedOn = true;
        //     battery-=5;
        // }
    }
    

    
}
