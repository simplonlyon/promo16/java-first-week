package co.simplon.promo16;

import java.util.List;

import co.simplon.promo16.bases.Conditions;
import co.simplon.promo16.bases.Looping;
import co.simplon.promo16.oop.Laptop;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // Conditions conditions = new Conditions();
        
        // System.out.println(conditions.skynetAI("Hello"));

        // Laptop laptop = new Laptop("Lenovo T440");
        // laptop.powerSwitch();
        // laptop.powerSwitch();
        // laptop.plug();
        // laptop.powerSwitch();
        // laptop.powerSwitch();


        Looping looping = new Looping();
        // looping.findMax(List.of(1,2,3,4,2,4,9,1,3));
        // String[] words = {"génial", "test", "bloup", "jean"};
        // looping.find(words, "blup");

        looping.averageChars("Test il de trois");
    }
}
